<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Response;
use Request;
use Carbon\Carbon;
use Mail;
use File;

class HomeController extends Controller
{
    

    public function index(){

        return view('index');
    }

    public function contactos(){

        return view('contactos.index');
    }

    public function hotel(){

        return view('hotel.index');
    }
    public function nosotros(){

        return view('nosotros.index');
    }

    public function tours(){

        return view('tours.index');
    }

    public function tour($id){

        $json = File::get("data/tours.json");

        $array = json_decode($json, true);

        $tour = collect($array[$id-1]);

        return view('tour.index', compact('tour'));
    }

    public function galeria(){

        return view('galeria.index');
    }

    public function correo()
    {        
        try {
            $data = Request::all();

        
            $data['entrada'] = Carbon::parse($data['entrada'])->format('d-m-Y h:i:s A');
            $data['salida'] = Carbon::parse($data['salida'])->format('d-m-Y h:i:s A');

            Mail::send('emails.contacto', ['data' => $data], function ($m) use ($data) {
                $m->from('info@sapitravel.com', 'Sapito Tours')
                    ->to('info@sapitravel.com', 'Sapito Tours')
                    ->cc('sapitotours@gmail.com')
                    ->cc('alvarado.websis@gmail.com')
                  ->subject('Página Web');
            });


            return Response::json($data, 200, array('content-type' => 'application/json', 'Access-Control-Allow-Origin' => '*'));
        } catch (Exception $e) {
            return Response::json($data, 500, array('content-type' => 'application/json', 'Access-Control-Allow-Origin' => '*'));
        }

    }

}
