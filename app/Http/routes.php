<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// Route::get('/', function () {
//     return view('index');
// });

Route::group(['middleware' => ['cors']], function() {
    
Route::get('/', 'HomeController@index')->name('home');
Route::get('/contactos', 'HomeController@contactos')->name('contactos');
// Route::get('/hotel', 'HomeController@hotel')->name('hotel');
Route::get('/tours', 'HomeController@tours')->name('tours');
Route::get('/nosotros', 'HomeController@nosotros')->name('nosotros');
Route::get('/tour/{name}', 'HomeController@tour')->name('tour');
Route::get('/galeria', 'HomeController@galeria')->name('galeria');
    Route::post('/correo', 'HomeController@correo')->name('correo');
});
Route::get('/correo', function(){ 

    // Info
    $data['email'] = "alvarado@gmail.com";
    $data['entrada'] =  "Fri Aug 26 2016 00:00:00 GMT-0600 (Central America Standard Time)";
    $data['habitacion'] = "Delux";
    $data['nombre'] = "Alvarado";
    $data['nota'] = "Ninguna";
    $data['personas'] =  "1";
    $data['salida'] =  "Fri Aug 26 2016 00:00:00 GMT-0600 (Central America Standard Time)";
    $data['servicio'] = "Tour y Hotel";
    $data['tour'] = "Lago";

    return view('emails.contacto', compact('data')); 
});
