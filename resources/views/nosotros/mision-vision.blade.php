<div id="fh5co-testimonial" ng-controller="ClientesCtrl">
    <div class="container">
        <div class="row mision">
            <div class="col-md-10 col-md-offset-1 text-center fh5co-heading animate-box" data-animate-effect="fadeIn">
                <h2>Te saluda</h2>
                <img src="/images/logo.jpg" alt="Logotipo del Sapito" width="40%">
                <p>
                    Una empresa Tour operadora Local que se dedica a ofrecer servicios de tours Guiados en la ciudad de Suchitoto ofreciendo experiencias únicas para que los turistas en nuestro país disfruten de la mejor manera su estadía.
                </p>
            </div>
            <div class="col-md-4 text-center item-block">
                <blockquote>
                    <h2>Objetivo</h2>
                    <p>
                    Promover el turismo local y Nacional ofreciendo servicios innovadores y divertidos para que el turista pase entretenido en nuestro país.
                    </p>
                </blockquote>
            </div>
            <div class="col-md-4 text-center item-block">
                <blockquote>
                    <h2>Misión</h2>
                    <p>
                    Ofrecer servicios turísticos de calidad al turista que lo solicite con una atención profesional y personalizada dentro del país por medio de las diferentes actividades turísticas que se ofrezcan en la zona teniendo experiencias únicas utilizando todos los recursos humanos, materiales y naturales promoviendo un turismo sostenible en diferentes comunidades o pueblos de nuestro territorio.
                    </p>
                </blockquote>
            </div>
            <div class="col-md-4 text-center item-block">
                <blockquote>
                    <h2>Visión</h2>
                    <p>
                    Ser el tour operador referente de la zona como una empresa responsable y confiable, poniendo en práctica las normas de calidad para la satisfacción del cliente.
                    </p>
                </blockquote>
            </div>
        </div>
    </div>
</div>