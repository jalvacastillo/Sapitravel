<footer id="fh5co-footer" role="contentinfo">

    <div class="container">
        <div class="col-md-4 col-sm-12">
            <h3>Sobre nosotros</h3>
            <p>
                Sapito tours una empresa Tour operadora Local que se dedica a ofrecer servicios de tours Guiados en la ciudad de Suchitoto ofreciendo experiencias únicas para que los turistas en nuestro país disfruten de la mejor manera su estadía.
            </p>
            <p><a href="{{ route('contactos') }}" class="btn btn-primary btn-outline with-arrow btn-sm">Contactanos <i class="icon-arrow-right"></i></a></p>
        </div>
        <div class="col-md-2 col-sm-6">
            <h3>Servicios</h3>
            <ul class="float">
                <li><a href="{{ route('nosotros') }}">Nosotros</a></li>
                {{-- <li><a href="{{ route('hotel') }}">Hotel</a></li> --}}
                <li><a href="{{ route('tours') }}">Tours</a></li>
                <li><a href="{{ route('galeria') }}">Galeria</a></li>
            </ul>
        </div>

        <div class="col-md-3 col-sm-6">
            <h3>Contactos</h3>
            <ul class="float">
                <li class="call"><a href="tel://+5037504-5490">
                <i class="glyphicon glyphicon-phone"></i> +(503) 7504-5490</a></li>
                <li class="cta"><a href="mailto:info@sapitravel.com">
                <i class="glyphicon glyphicon-envelop"></i> info@sapitravel.com</a></li>
                <li class="cta"><a href="#">
                <i class="glyphicon glyphicon-location"></i> Suchitoto, Cuscatlan, El Salvador</a></li>
            </ul>
        </div>

        <div class="col-md-3 col-sm-12 text-center">
            <h3>Siguenos</h3>
            <ul class="fh5co-social">
                <li><a href="https://twitter.com/SapitoTours"><i class="icon-twitter"></i></a></li>
                <li><a href="https://www.facebook.com/Sapito-Tours-1054772717901381"><i class="icon-facebook"></i></a></li>
                <li><a href="https://www.youtube.com/user/SapitoTours"><i class="icon-youtube"></i></a></li>
                <li><a href="https://www.instagram.com/sapito_tours/"><i class="icon-instagram"></i></a></li>
            </ul>
        </div>
    </div>
    
    <div class="container-fluid">
        <div class="col-md-12 fh5co-copyright text-center">
            <img src="{{ asset('images/marca_pais.png') }}" height="150"/>
            <p>&copy; 2017 Sapitos Tour 
                <span> Realizada por <a href="http://cri.catolica.edu.sv/cdmype" target="_blank">CDMYPE Ilobasco</a> 
                </span>
            </p>    
        </div>
    </div>
</footer>