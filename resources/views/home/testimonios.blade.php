<div id="fh5co-testimonial" ng-controller="ClientesCtrl">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3 text-center fh5co-heading animate-box" data-animate-effect="fadeIn">
                <h2>Testimonio de clientes</h2>
                <p>
                    Nuestros clientes se divierten, conocen y aprenden con todos nuestros tours
                </p>
            </div>
            <div class="col-md-4 text-center item-block" ng-repeat="cliente in clientes">
                <blockquote>
                    <p>&ldquo; <span ng-bind="cliente.testimonio"></span> &rdquo;</p>
                    <p><span class="fh5co-author">
                        <cite ng-bind="cliente.nombre"></cite>
                        </span><i class="icon facebook-color pull-right"><span ng-bind="cliente.tipo"></span></i>
                    </p>

                </blockquote>
            </div>
        </div>
    </div>
</div>