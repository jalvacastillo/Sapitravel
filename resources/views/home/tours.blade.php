<div id="best-deal" ng-controller="ToursCtr">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2 text-center fh5co-heading animate-box" data-animate-effect="fadeIn">
                <h2>Nuestros tours</h2>
                <p>Te ofrecemos los mejores tours de Suchitoto.</p>
            </div>
            
            <div class="col-sm-6 col-md-4 item-block" ng-repeat="tour in tours | limitTo:3">

                <div class="fh5co-property">
                    <figure>
                        <img ng-src="/images/tours/@{{tour.img}}" class="img-responsive">
                    </figure>
                    <div class="fh5co-property-innter">
                        <h3><a href="#" ng-bind="tour.nombre"></a></h3>
                        <div class="price-status">
                            <span class="price"><span ng-bind="tour.precio | currency"></span>
                                <span class="per">/ Persona</span> 
                            </span>
                            <br>
                            <span ng-bind="tour.min"></span> personas como mínimo.
                        </div>
                   <p ng-bind="tour.descripcion | limitTo:85">
                       ...
                   </p>
                </div>
                <p class="fh5co-property-specification">
                    <a href="tour/@{{tour.id}}" class="btn btn-block btn-primary">Ver más</a>
                </p>
                </div>

            </div>

            <div class="col-md-12 text-center animate-box" data-animate-effect="fadeIn">
                <p><a href="{{ route('tours') }}" class="btn btn-primary btn-outline with-arrow">Ver todos los tours <i class="icon-arrow-right"></i></a></p>
            </div>

        </div>
    </div>
</div>