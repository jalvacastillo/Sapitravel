<aside id="fh5co-hero" clsas="js-fullheight">
    <div class="flexslider js-fullheight">
        <ul class="slides">

        {{-- Tours --}}
        <li style="background-image: url(/images/slide_1.jpg);">
            <div class="container">
                <div class="col-md-12 text-center js-fullheight fh5co-property-brief slider-text">
                    <div class="fh5co-property-brief-inner">
                        <div class="fh5co-box">
                            
                            <h3><a href="#">Descubre lo mejor de El Salvador </a></h3>
                            <div class="price-status">
                                <span class="price">Con nuestros <a href="{{ route('tours') }}" class="tag">12 tours</a></span>
                            </div>
                            <p>
                                Vive una excelente aventura, disfruta de una deliciosa gastronomía, conoce nuestra cultura y un sin fin de lugares fabulosos siempre acompañado por un experto en turismo.
                            </p>

                        <p class="fh5co-property-specification">
                            <span><strong>12</strong> Tours</span>
                            <span><strong>12</strong> Aventuras</span>
                            <span><strong>23+</strong> Lugares</span>
                        </p>

                        <p><a href="{{ route('tours') }}" class="btn btn-primary">Vea nuestros tours</a></p>
                                
                        
                        </div>
                    </div>
                </div>
            </div>
        </li>

        {{-- Hotel --}}
        <li style="background-image: url(/images/slide_2.jpg);">
            <div class="container">
                <div class="col-md-12 text-center js-fullheight fh5co-property-brief slider-text">
                    <div class="fh5co-property-brief-inner">

                        <div class="fh5co-box" style="float:right;">
                            <h3><a href="{{ route('contactos') }}">Conoce Suchitoto</a></h3>
                            <div class="price-status">
                            <span class="price"> <a href="{{ route('contactos') }}" class="tag">Con nosotros</a></span>
                        </div>
                        <p>
                            Visita Suchitoto, un pueblo encantador donde disfrutas de espectaculares paisajes y de la amabilidad de su gente.
                        </p>
                        <p class="fh5co-property-specification">
                            <span><strong>Incluimos</strong> Transporte</span>
                            <span><strong></strong> Guía</span>
                            <span><strong></strong> Agua</span>
                        </p>
                        <p><a href="{{ route('contactos') }}" class="btn btn-primary">Contactanos</a></p>
                        
                        </div>
                    </div>
                </div>
            </div>
        </li>

        </ul>
    </div>
</aside>