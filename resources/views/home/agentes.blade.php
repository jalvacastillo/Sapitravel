
<div id="fh5co-agents">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3 text-center fh5co-heading white animate-box" data-animate-effect="fadeIn">
                <h2>Nuestro equipo</h2>
                <p> Contamos con un equipo altamente profesional y confiable.
                </p>
            </div>
            <div class="col-md-4 text-center item-block animate-box" data-animate-effect="fadeIn">

                <div class="fh5co-agent">
                    <figure>
                        <img src="/images/testimonial_person2.jpg" alt="Free Website Template by FreeHTML5.co">
                    </figure>
                    <h3>John Doe</h3>
                    <p>Veniam laudantium rem odio quod, beatae voluptates natus animi fugiat provident voluptatibus. Debitis assumenda, possimus ducimus omnis.</p>
                    <p><a href="#" class="btn btn-primary btn-outline">Contact Me</a></p>
                </div>
                
            </div>
            <div class="col-md-4 text-center item-block animate-box" data-animate-effect="fadeIn">
                <div class="fh5co-agent">
                    <figure>
                        <img src="/images/testimonial_person3.jpg" alt="Free Website Template by FreeHTML5.co">
                    </figure>
                    <h3>John Doe</h3>
                    <p>Veniam laudantium rem odio quod, beatae voluptates natus animi fugiat provident voluptatibus. Debitis assumenda, possimus ducimus omnis.</p>
                    <p><a href="#" class="btn btn-primary btn-outline">Contact Me</a></p>
                </div>
            </div>
            <div class="col-md-4 text-center item-block animate-box" data-animate-effect="fadeIn">
                <div class="fh5co-agent">
                    <figure>
                        <img src="/images/testimonial_person4.jpg" alt="Free Website Template by FreeHTML5.co">
                    </figure>
                    <h3>John Doe</h3>
                    <p>Veniam laudantium rem odio quod, beatae voluptates natus animi fugiat provident voluptatibus. Debitis assumenda, possimus ducimus omnis.</p>
                    <p><a href="#" class="btn btn-primary btn-outline">Contact Me</a></p>
                </div>
            </div>
        </div>
    </div>
</div>