<div class="fh5co-cta" style="background-image: url(/images/accion.jpg);">
    <div class="overlay"></div>
    <div class="container">
        <div class="col-md-12 text-center animate-box" data-animate-effect="fadeIn">
            <h3>¿Estas listo para disfrutar de El Salvador?<br>
                <small>Nosotros estamos listos para mostrartelo</small>
            </h3>
            <p><a href="{{ route('contactos') }}" class="btn btn-primary btn-outline with-arrow">Iniciar! <i class="icon-arrow-right"></i></a></p>
        </div>
    </div>
</div>