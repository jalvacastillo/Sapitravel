<div id="fh5co-blog">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3 text-center fh5co-heading animate-box" data-animate-effect="fadeIn">
                <h2>Latest <em>from</em> Blog</h2>
                <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. </p>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-4 animate-box" data-animate-effect="fadeIn">
                <a class="fh5co-entry" href="#">
                    <figure>
                    <img src="/images/slide_4.jpg" alt="Free Website Template, Free HTML5 Bootstrap Template" class="img-responsive">
                    </figure>
                    <div class="fh5co-copy">
                        <h3>We Create Awesome Free Templates</h3>
                        <span class="fh5co-date">June 8, 2016</span>
                        <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. </p>
                    </div>
                </a>
            </div>
            <div class="col-md-4 animate-box" data-animate-effect="fadeIn">
                <a class="fh5co-entry" href="#">
                    <figure>
                    <img src="/images/slide_5.jpg" alt="Free Website Template, Free HTML5 Bootstrap Template" class="img-responsive">
                    </figure>
                    <div class="fh5co-copy">
                        <h3>Handcrafted Using CSS3 &amp; HTML5</h3>
                        <span class="fh5co-date">June 8, 2016</span>
                        <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. </p>
                    </div>
                </a>
            </div>
            <div class="col-md-4 animate-box" data-animate-effect="fadeIn">
                <a class="fh5co-entry" href="#">
                    <figure>
                    <img src="/images/slide_6.jpg" alt="Free Website Template, Free HTML5 Bootstrap Template" class="img-responsive">
                    </figure>
                    <div class="fh5co-copy">
                        <h3>We Try To Update The Site Everyday</h3>
                        <span class="fh5co-date">June 8, 2016</span>
                        <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. </p>
                    </div>
                </a>
            </div>
            <div class="col-md-12 text-center animate-box" data-animate-effect="fadeIn">
                <p><a href="#" class="btn btn-primary btn-outline with-arrow">View More Posts <i class="icon-arrow-right"></i></a></p>
            </div>
        </div>
    </div>  
</div>