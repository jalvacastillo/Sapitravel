<div class="fh5co-section-with-image">
    
    <img src="/images/vista.jpg" alt="" class="img-responsive">
    <div class="fh5co-box animate-box">
        <h2>Recorre el salvador <br>
            <small>En compañia de un experto.</small>
        </h2>
        <p>
            Nosotros te mostramos los mejores lugares de nuestro país, ofreciendo experiencias únicas.
        </p>
        <p><a href="{{ route('contactos') }}" class="btn btn-primary btn-outline with-arrow">Contactanos<i class="icon-arrow-right"></i></a></p>
    </div>
    
</div>