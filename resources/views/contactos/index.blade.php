@extends('base')

@section('content')

    @include('contactos.titulo')
    @include('contactos.formulario')

    <br><br><br><br>
    <div class="col-md-6 col-md-offset-3 text-center fh5co-heading animate-box" data-animate-effect="fadeIn">
        <h2>Encentranos en Suchitoto</h2>
        <p> Contamos con un equipo altamente profesional y confiable.
        </p>
    </div>
    
    <div id="map" class="animate-box" data-animate-effect="fadeIn"></div>
    
@endsection