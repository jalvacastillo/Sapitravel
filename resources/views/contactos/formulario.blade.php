<div class="animate-box">
    <div class="container">
        <div class="row">
            <div class="col-sm-8 col-sm-offset-2">
            <br><br>
                  <form ng-controller="CorreoCtrl" role="form" method="POST" ng-submit="email(correo);">
                        {{--  Servicio --}}
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Selecciona el Tour:</label>
                                <select class="form-control" ng-model="correo.tour">
                                    <option value="Caminatas En Suchitoto">
                                        $35 - Caminatas En Suchitoto
                                    </option>
                                    <option value="Sapociclos">
                                        $5 - Sapociclos
                                    </option>
                                    <option value="Bosque de Cinquera">
                                        $45 - Bosque de Cinquera
                                    </option>
                                    <option value="Suchitoto y Lago de Suchitlán">
                                        $42 - Suchitoto y Lago de Suchitlán
                                    </option>
                                    <option value="Cascada los Tercios">
                                        $15 - Cascada los Tercios
                                    </option>
                                    <option value="Parque Arqueológico Cihuatán">
                                        $35 - Parque Arqueológico Cihuatán
                                    </option>
                                    <option value="Cabalgatas en Guazapa">
                                        $55 - Cabalgatas en Guazapa
                                    </option>
                                    <option value="Artesanal ">
                                        $60 - Artesanal 
                                    </option>
                                    <option value="La Palma">
                                        $55 - La Palma
                                    </option>
                                    <option value="Ruta de las Flores">
                                        $90 - Ruta de las Flores
                                    </option>
                                    <option value="Aguacayo">
                                        $35 - Aguacayo
                                    </option>
                                    <option value="Cabalgatas en Suchitoto ">
                                        $25 - Cabalgatas en Suchitoto 
                                    </option>
                                    <option value="Sitio Arqueológico Ciudad Vieja">
                                        $35 - Sitio Arqueológico Ciudad Vieja
                                    </option>
                                    <option value="Cerro Verde y Lago de Coatepeque">
                                        $60 - Cerro Verde y Lago de Coatepeque
                                    </option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Cuantos nos visitarán:</label>
                                <input required class="form-control" min="1" type="number" value="1" ng-model="correo.personas">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Cuando inician:</label>
                                <input required class="form-control" type="datetime-local" ng-model="correo.entrada">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Cuando finalizan:</label>
                                <input required class="form-control" type="datetime-local" ng-model="correo.salida">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Con quien nos comunicamos:</label>
                                <input required class="form-control" placeholder="Nombre" type="text" ng-model="correo.nombre">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <input required class="form-control" placeholder="Correo" type="email" ng-model="correo.correo">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Algo que quiera agregar:</label>
                                <textarea name="" class="form-control" id="" cols="30" rows="2" placeholder="Nota" ng-model="correo.nota"></textarea>
                            </div>
                        </div>
                        <div class="col-md-12 text-center">
                            <div class="form-group">
                                <button class="btn btn-primary" type="submit" name="action">
                                  <span ng-show="!loader">Cotizar</span>
                                  <span ng-show="loader">Enviando...</span>
                                </button>
                            </div>
                        </div>
                        <div class="col-md-12 text-center" ng-show="errores">
                            <div class="alert alert-warning" role="alert">
                                <p> Upps... No se pudo enviar la cotización, intente nuevamente. </p>
                            </div>
                        </div>
                        <div class="col-md-12 text-center" ng-show="enviado">
                            <div class="alert alert-success" role="alert">
                                <p> Cotización enviada, Nos contactaremos lo antes posible con usted!! <br>
                                    Gracias por preferirnos!!!
                                </p>
                            </div>
                        </div>
                    </form>
            </div>
        </div>
    </div>  
</div>