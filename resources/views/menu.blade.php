<header id="fh5co-header" role="banner">
    <div class="container">
        <div class="row">
            <div class="header-inner">
                <h1><a href="{{ route('home') }}">
                    Sapito Tour<span>.</span></a>
                </h1>

                <nav role="navigation">
                    <ul>
                        <li><a href="{{ route('home') }}">Inicio</a></li>
                        {{-- <li><a href="{{ route('hotel') }}">Hotel</a></li> --}}
                        <li><a href="{{ route('tours') }}">Tours</a></li>
                        <li><a href="{{ route('nosotros') }}">Nosotros</a></li>
                        <li><a href="{{ route('galeria') }}">Galeria</a></li>
                        <li class="call"><a href="tel://+50375045490"><i class="icon-phone"></i> +(503) 7504-5490</a></li>
                        <li class="cta"><a href="{{ route('contactos') }}">Reservar</a></li>
                    </ul>
                </nav>
            </div>
        </div>
    </div>
</header>