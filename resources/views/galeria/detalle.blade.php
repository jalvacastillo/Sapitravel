<div id="best-deal" ng-controller='GaleriaCtrl'>
    <div class="container">
                
        <h2>Videos <a href="https://www.youtube.com/user/SapitoTours" class="btn btn-danger"> Siguenos <i class="icon-youtube"></i></a></h2>
        <div class="row">
            <div class="col-xs-4">
                <div class="embed-responsive embed-responsive-16by9">
                  <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/iqjILO8D2Bs" allowfullscreen></iframe>
                </div>
            </div>
            <div class="col-xs-4">
                <div class="embed-responsive embed-responsive-16by9">
                  <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/sjvnRf2zCzs" allowfullscreen></iframe>
                </div>
            </div>
            <div class="col-xs-4">
                <div class="embed-responsive embed-responsive-16by9">
                  <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/WJVkE9V48yk" allowfullscreen></iframe>
                </div>
            </div>
            
        </div>
        <br />
        <div class="row">
            <div class="col-xs-4">
                <div class="embed-responsive embed-responsive-16by9">
                  <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/hSiPpgOkCo0" allowfullscreen></iframe>
                </div>
            </div>
            
        </div>
        <div class="row">
            <br><br>
            <h2>Imagenes <a href="https://www.instagram.com/sapito_tours/" class="btn btn-primary"> Siguenos <i class="icon-instagram"></i></a></h2>
            <div id="lightgallery">
                <?php
                    $ruta = "images/galeria";
                    $filehandle = opendir($ruta);
                      while ($file = readdir($filehandle)) {
                            if ($file != "." && $file != "..") {
                                echo '<a href="'.$ruta."/".$file.'" class="col-md-4 col-xs-6" style="overflow: hidden; height: 300px; padding: 2px 4px;"> <img src="'.$ruta."/".$file.'" style="height: 100%;"/> </a>';
                            } 
                      } 
                    closedir($filehandle);
                ?>
            </div>

        </div>
    </div>
</div>
