@extends('base')

@section('content')

    @include('home.slider')
    @include('home.tours')
    @include('home.hotel')
    @include('home.testimonios')
    @include('home.accion')
    {{-- @include('home.blog') --}}

@endsection