<div class="container">
    <div class="row">
        <div class="col-md-12 item-block animate-box" data-animate-effect="fadeIn">
            <div class="fh5co-property">
                <div class="fh5co-property-innter">
                    <h3><a href="#">{{$tour['nombre']}}</a></h3>
                    <div class="price-status">
                        <span class="price">$ {{$tour['precio']}}<span class="per">/ Persona</span></span>
                    </div>
                    <p>
                    {!! $tour['descripcion'] !!}
                    </p>

                    <div role="tabpanel">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active">
                                <a href="#incluye" aria-controls="incluye" role="tab" data-toggle="tab">Incluye</a>
                            </li>
                           {{--  <li role="presentation">
                                <a href="#turismo" aria-controls="turismo" role="tab" data-toggle="tab">Turismo</a>
                            </li> --}}
                            <li role="presentation">
                                <a href="#horarios" aria-controls="horarios" role="tab" data-toggle="tab">Horarios</a>
                            </li>
                            <li role="presentation">
                                <a href="#precio" aria-controls="precio" role="tab" data-toggle="tab">Precio</a>
                            </li>
                            <li role="presentation">
                                <a href="#consejos" aria-controls="consejos" role="tab" data-toggle="tab">Consejos</a>
                            </li>
                        </ul>
                    
                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="incluye">
                                {!! $tour['incluye'] !!}
                            </div>
                           {{--  <div role="tabpanel" class="tab-pane" id="turismo">
                                {!! $tour['turismo'] !!}
                            </div> --}}
                            <div role="tabpanel" class="tab-pane" id="horarios">
                                {!! $tour['horarios'] !!}
                            </div>
                            <div role="tabpanel" class="tab-pane" id="precio">
                                $ {!! $tour['precio'] !!} dólares.
                            </div>
                            <div role="tabpanel" class="tab-pane" id="consejos">
                                {!! $tour['consejos'] !!}
                            </div>
                        </div>
                        <br>
                    </div>
                </div>
                
            </div>
        </div>
    </div>
    <br><br><br>
</div>