<div class="fh5co-page-title" style="background-image: url(/images/tours/{{$tour['img']}}); height:500px;">
    <div class="overlay"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-12 animate-box">
                <h1><span class="colored">Tour</span> {{ $tour['nombre']}}</h1>
            </div>
        </div>
    </div>
</div>