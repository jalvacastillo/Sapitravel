<div id="best-deal" ng-controller="CuartosCtrl" style="padding: 1em 0;">
    <div class="container">
        <div class="row">
            <br><br>
            <div class="col-md-6 item-block" ng-repeat="cuarto in cuartos">

                <div class="fh5co-property">
                    <figure>
                        <img ng-src="images/cuartos/@{{cuarto.img}}" class="img-responsive">
                        <h3 class="tag"><span ng-bind="cuarto.cuartos"></span> habitaciones <br> <small>Disponibles</small></h3>
                    </figure>
                    <div class="fh5co-property-innter">
                        {{-- <h3><a href="#">@{{cuarto.nombre}}</a></h3> --}}
                        <div class="price-status">
                        <span class="price" ng-bind="cuarto.precio | currency"></span>
                   </div>
                   <p ng-bind="cuarto.descripcion"></p>
                </div>
                <p class="fh5co-property-specification" style="height: 140px;">
                    <span class="col-xs-4" ng-repeat="item in cuarto.incluye"><span ng-bind="item.title"></span></span>
                </p>
                </div>

            </div>

            <div class="col-md-8 col-md-offset-2 text-center fh5co-heading animate-box" data-animate-effect="fadeIn">
                <p>
                    El lugar perfecto para descansar, con una vista al lago de Suchitlan impresionante que no querrás irte, ven y contacta con la naturaleza. ¡Tú y tu familia se lo merecen! <br><br>
                    Cuenta con cinco habitaciones que lo harán sentirse como en casa disfrutando de la tranquilidad que aquí nos rodea.

                </p>
            </div>
            <div class="col-md-12 text-center animate-box" data-animate-effect="fadeIn">
                <p><a href="{{ route('contactos') }}" class="btn btn-primary btn-outline with-arrow">Reservar<i class="icon-arrow-right"></i></a></p>
            </div>
            <div class="col-xs-6">
                <div id="lightgallery">
                    <?php
                        $ruta = "images/cuartos/uno";
                        $filehandle = opendir($ruta);
                          while ($file = readdir($filehandle)) {
                                if ($file != "." && $file != "..") {
                                    echo '<a href="'.$ruta."/".$file.'" style="padding:0;" class="col-md-4 col-xs-6"> 
                                        <img src="'.$ruta."/".$file.'" width="100%"/> </a>';
                                } 
                          } 
                        closedir($filehandle);
                    ?>
                </div>
            </div>
            <div class="col-xs-6">
                <div id="lightgallery">
                    <?php
                        $ruta = "images/cuartos/dos";
                        $filehandle = opendir($ruta);
                          while ($file = readdir($filehandle)) {
                                if ($file != "." && $file != "..") {
                                    echo '<a href="'.$ruta."/".$file.'" style="padding:0;" class="col-md-4 col-xs-6"> 
                                        <img src="'.$ruta."/".$file.'" width="100%"/> </a>';
                                } 
                          } 
                        closedir($filehandle);
                    ?>
                </div>
            </div>
            <div class="col-md-8 col-md-offset-2 text-center fh5co-heading animate-box" data-animate-effect="fadeIn">
            <br><br>
                <p>
                    Las tardes son maravillosas en el Hotel del Sapito, puedes reservar a cualquier hora y cualquier día, queremos que tus vacaciones sean para relajarte, compartirlas con la familia o los amigos. 
                </p>
            </div>

        </div>

    </div>
</div>