<!DOCTYPE html>
<html class="no-js" ng-app="app" lang="es">
    <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Sapito Tours</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Sapito Tours ofrece más de una docena de tours en El Salvador. Te ofrecemos en Suchitoto tours en una forma peculiar y divertida. Viajando por las calles empedradas de Suchitoto, Visitaran lugares de interés como El puerto San Juan, Iglesia Santa Lucia, galerías de arte, teatro de las ruinas y mucho más." />
    <meta name="keywords" content="suchitoto tours, hotel y tours, el salvador tours, guia turistico"/>
    <meta name="author" content="Sapitos Tours" />

    <!-- Facebook and Twitter integration -->
    <meta property="og:title" content=""/>
    <meta property="og:image" content=""/>
    <meta property="og:url" content=""/>
    <meta property="og:site_name" content=""/>
    <meta property="og:description" content=""/>
    <meta name="twitter:title" content="" />
    <meta name="twitter:image" content="" />
    <meta name="twitter:url" content="" />
    <meta name="twitter:card" content="" />

    <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
    <link rel="shortcut icon" href="/favicon.ico">
    <link href='https://fonts.googleapis.com/css?family=Varela+Round' rel='stylesheet' type='text/css'>
    
    <!-- Animate.css -->
    <link rel="stylesheet" href="{{ asset('css/animate.css') }}">
    <!-- Icomoon Icon Fonts-->
    <link rel="stylesheet" href="{{ asset('css/icomoon.css') }}">
    <!-- Bootstrap  -->
    <link rel="stylesheet" href="{{ asset('css/bootstrap.css') }}">
    <!-- Flexslider  -->
    <link rel="stylesheet" href="{{ asset('css/flexslider.css') }}">
    
    <!-- Theme style  -->
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">

    <link rel="stylesheet" href="{{ asset('lightGallery/css/lightgallery.min.css') }}">

    <!-- Modernizr JS -->
    <script src="{{ asset('js/modernizr-2.6.2.min.js') }}"></script>
    
    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-83309841-1', 'auto');
      ga('send', 'pageview');

    </script>

    </head>
    <body>
    
    
    <div id="fh5co-page">

        @include('menu')

        @yield('content')
        
        @include('footer')
    
    </div>
    
    
    <!-- jQuery -->
    <script src="{{ asset('js/jquery.min.js') }}"></script>
    <!-- jQuery Easing -->
    <script src="{{ asset('js/jquery.easing.1.3.js') }}"></script>
    <!-- Bootstrap -->
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>
    <!-- Waypoints -->
    <script src="{{ asset('js/jquery.waypoints.min.js') }}"></script>
    <!-- Flexslider -->
    <script src="{{ asset('js/jquery.flexslider-min.js') }}"></script>
    <script src="{{ asset('js/angular.min.js') }}"></script>
    <script src="{{ asset('js/controller.js') }}"></script>

    <script src="{{ asset('lightGallery/js/lightgallery.min.js') }}"></script>
    <script src="{{ asset('lightGallery/js/lg-thumbnail.min.js') }}"></script>
    <script src="{{ asset('lightGallery/js/lg-fullscreen.min.js') }}"></script>

    <!-- Google Map -->
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAgODF4XrrA4GQmQLqQooJVQBLgxtONC3M&sensor=false"></script>
    <script src="js/google_map.js"></script>

    <!-- MAIN JS -->
    <script src="{{ asset('js/main.js') }}"></script>
    <script>
        $(document).ready(function() {
                $("#lightgallery").lightGallery({thumbnail:true}); 
            });
    </script>

    </body>
</html>

